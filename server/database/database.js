const Sequelize = require('sequelize');

sequelize = new Sequelize(

        process.env.MYSQL_DATABASE,
        process.env.MYSQL_USER,
        process.env.MYSQL_PASSWORD,
        { 
            host : process.env.MYSQL_HOST,
            dialect : 'mysql',
            pool: {
                max: 10,
                min: 0,
                require:30000,
                idle:10000
            },
            logging: false
        }
);

module.exports = sequelize;
