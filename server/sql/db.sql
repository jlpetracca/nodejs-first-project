CREATE TABLE IF NOT EXISTS `store_tiendanube` (
  `id` bigint(20) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `scope` varchar(255) NOT NULL
);