module.exports = (sequelize, Sequelize) => {
  const Userfacebook = sequelize.define('userfacebook', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    name: Sequelize.STRING,
    surname: Sequelize.STRING,
    email: Sequelize.STRING,
    access_token: Sequelize.STRING
  }, {});
  Userfacebook.associate = function(models) {
    // associations can be defined here
  };
  return Userfacebook;
};

