module.exports = (sequelize, Sequelize) => {
  const Userstore = sequelize.define('userstore', {
    storetiendanubeId: {
      type: Sequelize.BIGINT,
      unique: 'store_user',
    },
    userfacebookId: {
      type: Sequelize.INTEGER,
      unique: 'store_user',
    },
  }, {});
  Userstore.associate = function(models) {
    // associations can be defined here
    Userstore.belongsTo(models.userfacebook);
    Userstore.belongsTo(models.storetiendanube);
  };
  return Userstore;
};
 
