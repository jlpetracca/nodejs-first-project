module.exports = (sequelize, Sequelize) => {
  var Storetiendanube = sequelize.define('storetiendanube', {
    id: {type: Sequelize.BIGINT,
    allowNull: false,
    primaryKey: true},
    access_token: {type: Sequelize.STRING,
      allowNull: false,
    },
    scope: {type: Sequelize.STRING,
      allowNull: false,

    }


  }, {});
  Storetiendanube.associate = function(models) {
    // associations can be defined here
  };
  return Storetiendanube;
};