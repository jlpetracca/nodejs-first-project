"use strict";
const path = require('path');
const Sequelize = require('sequelize');
var config = require(path.join(__dirname, '..', 'config', 'config.json'))[process.env.NODE_ENV];
const sequelize = new Sequelize(config.database, config.username, config.password, config);
var db = {};


var fs = require("fs");
fs
    .readdirSync(__dirname)
    .filter(function (file) {
        return (file.indexOf(".") !== 0) && (file !== "index.js");
    })
    .forEach(function (file) {
        var model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(function (modelName) {
    if ("associate" in db[modelName]) {
        db[modelName].associate(db);
    }
});



//Associations!!
db.storetiendanube.belongsToMany(db.userfacebook, {
    through: {
        model: db.userstore,
        unique: false,
    },
    foreignKey: 'storetiendanubeId',
    constraints: true
});
//db.userfacebook.belongsToMany(db.storetiendanube, {through: 'userstore'});

db.userfacebook.belongsToMany(db.storetiendanube, {
    through: {
        model: db.userstore,
        unique: false,
    },
    foreignKey: 'userfacebookId',
    constraints: true
});

/*

db.userstore.belongsTo(db.userfacebook);
db.userstore.belongsTo(db.storetiendanube);
*/

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;













