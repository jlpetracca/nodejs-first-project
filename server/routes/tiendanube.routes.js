const express = require('express');
const router = express.Router();

const tiendanube = require('../controllers/tiendanube.controller');

router.get ('/login', tiendanube.login );
router.get ('/auth', tiendanube.auth );

module.exports = router;   