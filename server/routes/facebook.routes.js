const express = require('express');
const passport = require('passport');

const router = express.Router();

const facebook = require('../controllers/facebook.controller');


router.get("/auth/", passport.authenticate("facebook", { scope: [ 'email'] }));

router.get("/auth/callback",
  passport.authenticate("facebook", {
    successRedirect: "/dashboard",
    failureRedirect: "/fail"
  })
);

router.get("/fail", (req, res) => {
  res.send("Failed attempt");
});

router.get("/success", (req, res) => {
  res.send("Success");
});




module.exports = router;   