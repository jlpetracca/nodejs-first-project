const express = require('express');
const router = express.Router();
const passport = require('passport');


router.get("/", (req, res, next) => {
  res.render('index');
}
);

router.get("/login", passport.authenticate("facebook", { scope: ['email'] }));

router.get("/fblogin/callback",
  passport.authenticate("facebook", {
    successRedirect: "/dashboard/init",
    failureRedirect: "/fail",
    passReqToCallback: false
  })
);


router.get("/logout", (req, res, next) => {
  req.logout();
  res.redirect("/");

}
);
/*
router.use((req, res, next) => {
  isAuthenticated(req, res, next);
  next();
});

function isAuthenticated(req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  else {
    res.redirect('/');
  };

};



*/




module.exports = router;   