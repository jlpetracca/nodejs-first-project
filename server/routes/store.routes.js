const express = require('express');
const router = express.Router();

const store = require('../controllers/store.controller');

router.get ('/', store.getStores );
router.post('/', store.createStores );
router.get('/:id', store.getStore );
router.put('/:id', store.editStore );
router.delete('/:id', store.deleteStore);

module.exports = router;   