const express= require('express');
const morgan= require('morgan'); 
const path = require('path');
const env = require('node-env-file'); // .env file
env(__dirname + '/.env');

const passport = require('passport');
const session = require('express-session');

//const flash = require ('connect-flash');






const app = express();



// Settings 
require('./config/passport');
const port = process.env.PORT || '3000';
app.set('port', port);

//View Engine Settings
var EJS  = require('ejs');
app.engine('.html', EJS.renderFile);
app.set('view engine','html')
app.set('views', path.join(__dirname,('views')));


// Middlewares
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended:false}));
app.use(session ({
    secret: process.env.SESSION_SECRET,
    
    resave: false,
    saveUninitialized: false
}));
//app.use(flash());
app.use(passport.initialize());
app.use(passport.session());   
/*app.use((req,res,next)=>{
    app.locals.message
});
*/

//Models
var models = require("./models");
   
//Sync Database
//models.sequelize.sync()
    //.then(function() {console.log('Nice! Database looks fine')})
    //.catch(function(err) {console.log(err, "Something went wrong with the Database Update!")});



// Routes
app.use(express.static(path.join(__dirname,'/views/statics')));
app.use('/',require('./routes/index'));
app.use('/dashboard',require('./routes/dashboard'));

//Routes - Logged IN
app.use('/tiendanube',require('./routes/tiendanube.routes'));
//app.use('/api/facebook',require('./routes/facebook.routes'));
//app.use('/api/store',require('./routes/store.routes'));



//Static Files


// Starting the server

app.listen(app.get('port'), () => {
    console.log('Server on port', app.get('port'))
});