const tiendanubeCtrl = {};
const models  = require('../models');
const storetiendanube = models.storetiendanube;



//Login de tienda nube e Instalacion
tiendanubeCtrl.login = (req, res) => {
  res.redirect(process.env.TIENDANUBE_APP_URL + process.env.TIENDANUBE_APP_CLIENT_ID + '/authorize');
};


tiendanubeCtrl.auth = async function (req, res) {

  try{

  //leo el codigo de tienda nube
  var code = req.query.code;

  //armo el post solicitando token
  const request = require('request-promise');
  //defino post
  var options = {
    'method': 'POST',
    'url': process.env.TIENDANUBE_APP_URL + 'authorize/token',
    formData: {
      'code': code,
      'client_id': process.env.TIENDANUBE_APP_CLIENT_ID,
      'client_secret': process.env.TIENDANUBE_APP_CLIENT_SECRET,
      'grant_type': 'authorization_code'
    }
  };
  //envio post
  var tiendanubeResponse = await request(options, async function (error, response) { });
  tiendanubeResponse = JSON.parse(tiendanubeResponse);

  console.log(tiendanubeResponse);
  //El dato de Tienda Nube es valido  
  if (typeof tiendanubeResponse.user_id != "number") {
    console.log(tiendanubeResponse);
    console.log('ERROR RESPONSE TIENDA NUBE');
  }
  else {
    //busco la tienda en la base de datos
    let store_db = await storetiendanube.findOne({
      where: {
        id: tiendanubeResponse.user_id
      }
    });
  if (store_db != null ) {
    console.log('TOKEN ACTUALIZADO');
    await storetiendanube.update({
      'access_token': tiendanubeResponse.access_token,
      'scope': tiendanubeResponse.scope},
      {where: {'id' : tiendanubeResponse.user_id
      }
    })
    }
    else {
      let newStoreTiendaNube = await storetiendanube.create({
        id: tiendanubeResponse.user_id,
        access_token: tiendanubeResponse.access_token,
        scope: tiendanubeResponse.scope
      });
      console.log('tienda creada');
    };
    req.session.store = '';
    req.session.save();
    res.redirect('la puta que te pario');
    
    /*

*/



  };
  }catch(e)
{
  res.json(e);
}
};



module.exports = tiendanubeCtrl;


