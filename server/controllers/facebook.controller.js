const FacebookCtrl = {};

const models = require('../models');
var userfacebook = models.userfacebook;
const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;

passport.serializeUser(function (user, done) {
    done(null, user.email);
});

passport.deserializeUser( async function (email, done) {
    const UserFacebook = await  userfacebook.findOne({
        where: {
            'email': email
        }
    });
    done(null, UserFacebook);
});

passport.use(
    new FacebookStrategy(
        {
            clientID: process.env.FACEBOOK_APP_ID,
            clientSecret: process.env.FACEBOOK_APP_SECRET,
            callbackURL: process.env.FACEBOOK_CALLBACK_URL,
            profileFields: ["email", "name"]
        },

        async function (accessToken, refreshToken, profile, done) {
            var profilefb = profile._json;
            //console.log(accessToken);
            var userDB = await userfacebook.findOne({
                where: {
                    'email': profilefb.email
                }
            });
            if (userDB) {
                console.log('USERFACEBOOK EXIST & UPDATING')
                await userfacebook.update({
                    'access_token': accessToken
                },
                    { where: { 'email': profilefb.email } });


                userUpdated = await userfacebook.findOne({
                    where: {
                        'email': profilefb.email
                    }
                });


                done(null, userUpdated);
            }
            else {
                console.log('USERFACEBOOK NOT EXIST & CREATING∫')
                newUser = await userfacebook.create({
                    'id': profilefb.id,
                    'access_token': accessToken,
                    'email': profilefb.email,
                    'name': profilefb.first_name,
                    'surname': profilefb.last_name
                });
                done(null, newUser);

            };
        }
    )
);




// new userModel(userData).save();
// #done(null, profile);
//done(userData.id);


module.exports = FacebookCtrl;