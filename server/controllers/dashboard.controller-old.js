
const dashboardCtrl = {};
const models = require('../models');
var userfacebook = models.userfacebook;
var userstore = models.userstore;


dashboardCtrl.init = async function (req, res) {

//si le falta tienda lo dirige al login
  if (req.session.store === 'missingstore') {
    return res.redirect('/tiendanube/login');
  };

//termina comprobaciones de session y carga autorefresh
  res.render('blank_autorefresh');

//chequea info del usuario en sesion
  if (!req.session.user) {
    req.session.user = await userfacebook.findOne({ where: { 'email': req.session.passport.user } });
  };
//busca info de userstore en sesion
  if (!req.session.store) {
    req.session.store = await userstore.findOne({ where: { 'userfacebookId': req.session.user.id } });

    if (req.session.store == null) {
      req.session.store = 'missingstore';
    }
    req.session.save();
  };
};









module.exports = dashboardCtrl;